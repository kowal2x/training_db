﻿using System;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Telerik.OpenAccess;
using Telerik.OpenAccess.FetchOptimization;
using Training.DataAccess.FluentModel;
using Training.Infrastructure;

namespace Training.DataAccess.Test
{
	[TestFixture]
	public class AssociationsRecognition : DatabaseTestBase
	{
		public override void Init()
		{
			ContextFactory._replaceWithMssql();
			base.Init();
		}

		[Test]
		public void basic_association_works()
		{
			var pid = 0;
			using (var ctx = ContextFactory.CreateContext())
			{
				var cat = new Category()
				{
					Name = "cat",
					Description = "wegre",
				};
				var prod = new Product()
				{
					Name = "aaaa",
					Description = "ddddd",
					Price = 2,
					Category = cat
				};
				ctx.Add(prod);
				ctx.SaveChanges();

				pid = prod.Id;
			}

			using (var ctx = ContextFactory.CreateContext())
			{
				var prod = ctx.GetAll<Product>().Single(e => e.Id == pid);
				Assert.NotNull(prod);
			}
		}

		[Test]
		public void select_n_plus_1()
		{
			using (var ctx = ContextFactory.CreateContext())
			{
				var builder = new StringBuilder();
				var cats = ctx.GetAll<Category>().ToList();
				Console.Write(DateTime.Now);
				foreach (var cat in cats)
				{
					builder.Append(cat.Name);
					builder.Append(string.Join(" ",
						cat.Products.Select(e => e.Name)));
				}
				Console.Write(DateTime.Now);
			}
		}

		[Test]
		public void avoid_select_n_plus_1()
		{
			using (var ctx = ContextFactory.CreateContext())
			{
				var builder = new StringBuilder();
				var cats = ctx.GetAll<Category>()
					.Include(e => e.Products)
					.ToList();
				Console.Write(DateTime.Now);
				foreach (var cat in cats)
				{
					builder.Append(cat.Name);
					builder.Append(string.Join(" ",
						cat.Products.Select(e => e.Name)));
				}
				Console.Write(DateTime.Now);
			}
		}

		[Test]
		public void avoid2_select_n_plus_1()
		{
			using (var ctx = ContextFactory.CreateContext())
			{
				var fs = new FetchStrategy();
				//fs.LoadWith<smth>(c => c.prop);
				fs.LoadWith<Category>(c => c.Products);

				var builder = new StringBuilder();
				var cats = ctx.GetAll<Category>()
					.LoadWith(fs)
					.ToList();
				Console.Write(DateTime.Now);
				foreach (var cat in cats)
				{
					builder.Append(cat.Name);
					builder.Append(string.Join(" ",
						cat.Products.Select(e => e.Name)));
				}
				Console.Write(DateTime.Now);
			}
		}

		[Test]
		public void filter_sort_refs()
		{
			using (var ctx = ContextFactory.CreateContext())
			{
				var fs = new FetchStrategy();
				fs.LoadWith<Category>(c => c.Products);

				var cats = ctx.GetAll<Category>()
					.LoadWith(fs)
					.Select(e => new
					{
						Cat = e,
						Prods = e.Products.Where(f => f.Name.StartsWith("a"))
					})
					.ToList();
			}
		}
	}
}