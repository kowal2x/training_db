﻿using System.Linq;
using NUnit.Framework;
using Training.DataAccess.FluentModel;
using Training.Infrastructure;

namespace Training.DataAccess.Test
{
	[TestFixture]
	public class FluentModelRecognition : DatabaseTestBase
	{
		[Test]
		public void initialize_and_add_something()
		{
			int pid = 0;
			using (var ctx = ContextFactory.CreateContext())
			{
				var prod = new Product()
				{
					Name = "czapka",
					Description = "z naklejka",
					Price = 10
				};
				ctx.Add(prod);
				ctx.SaveChanges();
				pid = prod.Id;
			}
			using (var ctx = ContextFactory.CreateContext())
			{
				Assert.NotNull(
					ctx.GetAll<Product>().SingleOrDefault(e => e.Id == pid)
				);
			}
		}

		[Test]
		public void big_data()
		{
			string str = new string('a', 1024*1024*64);
			int pid = 0;
			using (var ctx = ContextFactory.CreateContext())
			{
				var prod = new Product()
				{
					Name = "czapka",
					Description = str,
					Price = 10
				};
				ctx.Add(prod);
				ctx.SaveChanges();
				pid = prod.Id;
			}
			using (var ctx = ContextFactory.CreateContext())
			{
				var prod = ctx.GetAll<Product>().Single(e => e.Id == pid);
				Assert.AreEqual(str, prod.Description);
			}
		}
	}
}