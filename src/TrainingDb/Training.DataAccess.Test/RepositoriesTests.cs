﻿using System.Linq;
using NUnit.Framework;
using Training.Core.Dto;
using Training.Core.Repositories;
using Training.Infrastructure;

namespace Training.DataAccess.Test
{
	[TestFixture]
	public class RepositoriesTests : DatabaseTestBase
	{
		public override void Init()
		{
			ContextFactory._removeSqliteDb();
			base.Init();
		}

		[Test]
		public void can_manipulate_categories()
		{
			var repo = new CategoryRepository();

			var dto = new CategoryDto()
			{
				Name = "a",
				Description = "b",
			};
			repo.Add(dto);
			Assert.AreNotEqual(0, dto.Id);
			var cats1 = repo.GetCategories();
			Assert.AreEqual(1, cats1.Count());
			Assert.AreEqual("a", cats1.First().Name);
			Assert.AreEqual("b", cats1.First().Description);

			dto.Name = "c";
			dto.Description = "d";
			repo.Update(dto);
			var cats2 = repo.GetCategories();
			Assert.AreEqual(1, cats2.Count());
			Assert.AreEqual("c", cats2.First().Name);
			Assert.AreEqual("d", cats2.First().Description);

			repo.Delete(dto);
			Assert.AreEqual(0, repo.GetCategories().Count());
		}
	}
}