﻿using NUnit.Framework;
using Training.Infrastructure;

namespace Training.DataAccess.Test
{
	[TestFixture]
	public class DatabaseTestBase
	{
		[SetUp]
		public virtual void Init()
		{
			using (var ctx = ContextFactory.CreateContext())
			{
				string script = null;
				var schemaHandler = ctx.GetSchemaHandler();
				if (schemaHandler.DatabaseExists() == false)
				{
					script = schemaHandler.CreateDDLScript();
					schemaHandler.CreateDatabase();
				}
				else
					script = schemaHandler.CreateUpdateDDLScript(null);

				if (string.IsNullOrWhiteSpace(script) == false)
					schemaHandler.ExecuteDDLScript(script);
			}
		}

		[TearDown]
		public virtual void Cleanup()
		{
			ContextFactory._revertToDefault();
		}
	}
}