﻿using System;
using System.Data.SqlClient;
using System.Transactions;
using NUnit.Framework;

namespace Training.Ado
{
	[TestFixture]
	public class SimpleRecognition
	{
		public void simple_open_check()
		{
			using (var conn = SqlConnectionFactory.CreateConnection())
			{
				conn.Open();
				var cmd = new SqlCommand("select * from Genres", conn);
				using (var rdr = cmd.ExecuteReader())
				{
					while (rdr.Read())
					{
						Console.WriteLine(rdr[0]);
						Console.WriteLine(rdr[1]);
					}
				}
			}
		}

		[Test]
		public void adds_last_and_reads()
		{
			const string tval1 = "qwe";
			const string tval2 = "rty";

			using (var ts = new TransactionScope())
			{
				using (var conn = SqlConnectionFactory.CreateConnection())
				{
					conn.Open();

					var addCmd = new SqlCommand(
						string.Format(
							"INSERT INTO Genres (Name, Description) VALUES ('{0}', '{1}')", tval1, tval2),
						conn);
					addCmd.ExecuteNonQuery();

					var cmd = new SqlCommand("select top 1 * from Genres order by GenreId desc", conn);
					using (var rdr = cmd.ExecuteReader())
					{
						rdr.Read();
						var val1 = (string) rdr[1];
						var val2 = (string) rdr[2];
						Assert.AreEqual(val1, tval1);
						Assert.AreEqual(val2, tval2);
					}
				}

				//ts.Complete();
			}
		}
	}
}