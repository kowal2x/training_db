﻿using System.Collections.Generic;
using Simple.Data;

namespace Training.Ado.Repository
{
	class OrdersRepositorySimpleData : IOrdersRepository
	{
		public void Add(OrderDto dto)
		{
			dynamic db = Database.OpenConnection(SqlConnectionFactory.ConnectionString);
			db.Orders.Insert(dto);
		}

		public void Update(OrderDto dto)
		{
			dynamic db = Database.OpenConnection(SqlConnectionFactory.ConnectionString);
			db.Orders.Update(dto);
		}

		public void Delete(OrderDto dto)
		{
			dynamic db = Database.OpenConnection(SqlConnectionFactory.ConnectionString);
			//db.Orders.Delete(dto);
			db.Orders.DeleteByOrderId(dto.OrderId);
		}

		public IEnumerable<OrderDto> GetAllOrderedByDateDesc()
		{
			dynamic db = Database.OpenConnection(SqlConnectionFactory.ConnectionString);
			return db.Orders.OrderByOrderDateDescending().ToArray<OrderDto>();
		}

		public IEnumerable<OrderDto> GetByCity(string cityName)
		{
			//dynamic db = Database.OpenConnection(SqlConnectionFactory.ConnectionString);
			//return db.Orders.All()
			//	.Where(db.Orders.City == cityName)
			//	.OrderByOrderDateDescending().ToArray<OrderDto>();
			dynamic db = Database.OpenConnection(SqlConnectionFactory.ConnectionString);
			return db.Orders.All()
				.Where(db.Orders.City == cityName)
				.OrderByDescending(db.Orders.OrderId).ToArray<OrderDto>();
		}
	}
}