﻿using System.Collections.Generic;

namespace Training.Ado.Repository
{
	public interface IOrdersRepository
	{
		void Add(OrderDto dto);
		void Update(OrderDto dto);
		void Delete(OrderDto dto);

		IEnumerable<OrderDto> GetAllOrderedByDateDesc();
		IEnumerable<OrderDto> GetByCity(string cityName);

		//void AddDetails
		//void MarkAsRealized
	}
}