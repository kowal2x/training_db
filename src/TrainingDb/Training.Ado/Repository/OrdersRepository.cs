﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace Training.Ado.Repository
{
	class OrdersRepository : IOrdersRepository
	{
		public void Add(OrderDto dto)
		{
			using (var conn = SqlConnectionFactory.CreateConnection())
			{
				conn.Open();
				var cmd = new SqlCommand(
					"INSERT INTO Orders (OrderDate, Username, FirstName, Total, City) VALUES (@Date, @Username, @FirstName, @Total, @City)",
					conn);
				cmd.Parameters.AddRange(
					new[]
					{
						new SqlParameter("Date", new SqlDateTime(dto.OrderDate)),
						new SqlParameter("Username", dto.Username),
						new SqlParameter("FirstName", dto.FirstName),
						new SqlParameter("Total", dto.Total),
						new SqlParameter("City", dto.City),
					});
				cmd.ExecuteNonQuery();
			}
		}

		public void Update(OrderDto dto)
		{
			using (var conn = SqlConnectionFactory.CreateConnection())
			{
				conn.Open();
				var cmd = new SqlCommand(
					"Update Orders set OrderDate=@Date, Username=@Username, FirstName=@FirstName, Total=@Total, City=@City where OrderId=@id",
					conn);
				cmd.Parameters.AddRange(
					new[]
					{
						new SqlParameter("id", dto.OrderId),
						new SqlParameter("Date", new SqlDateTime(dto.OrderDate)),
						new SqlParameter("Username", dto.Username),
						new SqlParameter("FirstName", dto.FirstName),
						new SqlParameter("Total", dto.Total),
						new SqlParameter("City", dto.City)
					});
				cmd.ExecuteNonQuery();
			}
		}

		public void Delete(OrderDto dto)
		{
			using (var conn = SqlConnectionFactory.CreateConnection())
			{
				conn.Open();
				var cmd = new SqlCommand(
					"delete from Orders where OrderId=@id",
					conn);
				cmd.Parameters.AddRange(
					new[]
					{
						new SqlParameter("id", dto.OrderId),
					});
				cmd.ExecuteNonQuery();
			}
		}

		public IEnumerable<OrderDto> GetAllOrderedByDateDesc()
		{
			using (var conn = SqlConnectionFactory.CreateConnection())
			{
				conn.Open();
				var cmd = new SqlCommand("select OrderId, OrderDate, Username, FirstName, Total, City from Orders order by OrderDate desc", conn);
				using (var rdr = cmd.ExecuteReader())
				{
					while (rdr.Read())
					{
						yield return new OrderDto()
						{
							OrderId = (int)rdr[0],
							OrderDate = (DateTime)rdr[1],
							Username = rdr[2] as string,
							FirstName = rdr[3] as string,
							Total = (decimal)rdr[4],
							City = rdr[5] as string,
						};
					}
				}
			}
		}

		public IEnumerable<OrderDto> GetByCity(string cityName)
		{
			using (var conn = SqlConnectionFactory.CreateConnection())
			{
				conn.Open();
				var cmd = new SqlCommand(@"select OrderId, OrderDate, Username, FirstName, Total, City from Orders where City=@city", conn);
				cmd.Parameters.AddRange(
					new[]
					{
						new SqlParameter("city", cityName),
					});
				using (var rdr = cmd.ExecuteReader())
				{
					while (rdr.Read())
					{
						yield return new OrderDto()
						{
							OrderId = (int)rdr[0],
							OrderDate = (DateTime)rdr[1],
							Username = rdr[2] as string,
							FirstName = rdr[3] as string,
							Total = (decimal)rdr[4],
							City = rdr[5] as string,
						};
					}
				}
			}
		}
	}
}