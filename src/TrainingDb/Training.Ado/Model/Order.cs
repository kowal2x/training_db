﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Training.Ado.Model
{
	public class Order
	{
		public int OrderId { get; set; }
		public DateTime OrderDate { get; set; }
		public string Username { get; set; }
		public string FirstName { get; set; }
		public decimal Total { get; set; }
		public string City { get; set; }

		public IEnumerable<OrderDetails> OrderDetails { get; set; }
	}
}