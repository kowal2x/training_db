﻿namespace Training.Ado.Model
{
	public class OrderDetails
	{
		public int OrderDetailId { get; set; }
		public decimal UnitPrice { get; set; }
	}
}