﻿using System.Data.SqlClient;

namespace Training.Ado
{
	public static class SqlConnectionFactory
	{
		public const string ConnectionString = @"Server=.\SQLEXPRESS;Database=MvcMusicStore;Trusted_Connection=True;";

		public static SqlConnection CreateConnection()
		{
			return new SqlConnection(ConnectionString);
		}
	}
}