﻿using System;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using NUnit.Framework;

namespace Training.Ado
{
	[TestFixture]
	public class SecurityRecognition
	{
		[Test]
		public void simple_open_check()
		{
			AddOrderCrappySqlInjectableDontDoThisPlease("aaa");
			using (var conn = SqlConnectionFactory.CreateConnection())
			{
				conn.Open();
				var cmd = new SqlCommand("select top 1 * from Orders order by OrderId desc", conn);
				using (var rdr = cmd.ExecuteReader())
				{
					rdr.Read();
					Assert.AreEqual(rdr[2], "aaa");
				}
			}
		}

		[Test]
		public void hack_this_crap()
		{
			//AddOrderCrappySqlInjectableDontDoThisPlease(@"a', '0')"
			//	+"drop table OrderDetails"
			//	+"--");
		}

		[Test]
		public void better_command_usage()
		{
			AddOrderBetter("stefan");
			using (var conn = SqlConnectionFactory.CreateConnection())
			{
				conn.Open();
				var cmd = new SqlCommand("select top 1 * from Orders order by OrderId desc", conn);
				using (var rdr = cmd.ExecuteReader())
				{
					rdr.Read();
					Assert.AreEqual(rdr[2], "stefan");
				}
			}
		}

		void AddOrderCrappySqlInjectableDontDoThisPlease(string customerName)
		{
			using (var conn = SqlConnectionFactory.CreateConnection())
			{
				conn.Open();

				var addCmd = new SqlCommand(
					string.Format(
						"INSERT INTO Orders (OrderDate, Username, Total) VALUES ('{0}', '{1}', '{2}')",
							DateTime.Now.ToShortDateString(), customerName, 0),
					conn);
				addCmd.ExecuteNonQuery();
			}
		}

		void AddOrderBetter(string customerName)
		{
			using (var conn = SqlConnectionFactory.CreateConnection())
			{
				conn.Open();

				var addCmd = new SqlCommand(
					"INSERT INTO Orders (OrderDate, Username, Total) VALUES (@Date, @Username, @Total)",
					conn);
				addCmd.Parameters.AddRange(
					new[]
					{
						new SqlParameter("Date", new SqlDateTime(DateTime.Now)),
						new SqlParameter("Username", customerName),
						new SqlParameter("Total", new SqlInt64(0))
					});
				addCmd.ExecuteNonQuery();
			}
		}
	}
}