﻿using NUnit.Framework;
using Simple.Data;
using Training.Ado.Model;

namespace Training.Ado
{
	[TestFixture]
	public class SimpleDataRecognition
	{
		[Test]
		public void samples_work()
		{
			dynamic db = Database.OpenConnection(SqlConnectionFactory.ConnectionString);

			string city = db.Orders.Get(100).City;
			Assert.That(city, Is.Not.Null);

			var orders = db.Orders.All()
				.Join(db.OrderDetails)
				.On(db.Orders.OrderId == db.OrderDetails.OrderId)
				.Select(
					db.Orders.Username,
					db.OrderDetails.UnitPrice
				);
			int k;
			k = 2;
		}
	}
}