﻿using System;
using System.Linq;
using System.Transactions;
using NUnit.Framework;
using Training.Ado.Repository;

namespace Training.Ado
{
	[TestFixture]
	public class OrdersRepositoryTests
	{
		private TransactionScope _ts;
		[SetUp]
		public void Initialize()
		{
			_ts = new TransactionScope();
		}
		[TearDown]
		public void Cleanup()
		{
			_ts.Dispose();
			_ts = null;
		}

		IOrdersRepository CreateRepo()
		{
			return new OrdersRepositorySimpleData();
		}

		[Test]
		public void adds_new_order_properly()
		{
			IOrdersRepository repo = CreateRepo();
			repo.Add(new OrderDto()
			{
				OrderDate = DateTime.Now,
				Username = "me",
				Total = 0,
				FirstName = "Janek",
				City = "NYC"
			});

			var lastAdded = repo.GetAllOrderedByDateDesc().First();
			Assert.AreEqual("me", lastAdded.Username);
			Assert.AreEqual(0, lastAdded.Total);
			Assert.AreEqual("Janek", lastAdded.FirstName);
			Assert.AreEqual("NYC", lastAdded.City);
		}

		[Test]
		public void updates_order_properly()
		{
			IOrdersRepository repo = CreateRepo();
			repo.Add(new OrderDto()
			{
				OrderDate = DateTime.Now,
				Username = "me",
				Total = 0,
				FirstName = "Janek",
				City = "NYC"
			});

			var lastAdded = repo.GetAllOrderedByDateDesc().First();
			lastAdded.Username = "zdzichu";
			repo.Update(lastAdded);

			var lastAddedRetrieved = repo.GetAllOrderedByDateDesc().First();
			Assert.AreEqual("zdzichu", lastAddedRetrieved.Username);
		}

		[Test]
		public void deletes_order_properly()
		{
			IOrdersRepository repo = CreateRepo();
			repo.Add(new OrderDto()
			{
				OrderDate = DateTime.Now,
				Username = "me",
				Total = 0,
				FirstName = "Janek",
				City = "NYC"
			});

			var cnt1 = repo.GetAllOrderedByDateDesc().Count();
			var lastAdded = repo.GetAllOrderedByDateDesc().First();
			repo.Delete(lastAdded);
			var cnt2 = repo.GetAllOrderedByDateDesc().Count();

			Assert.AreEqual(cnt1, cnt2 + 1);
		}

		[Test]
		public void gets_by_city_properly()
		{
			IOrdersRepository repo = CreateRepo();
			repo.Add(new OrderDto()
			{
				OrderDate = DateTime.Now,
				Username = "me",
				Total = 0,
				FirstName = "Janek",
				City = "my_fake_city"
			});

			var cnt1 = repo.GetByCity("my_fake_city").Count();
			repo.Add(new OrderDto()
			{
				OrderDate = DateTime.Now,
				Username = "me",
				Total = 0,
				FirstName = "Janek",
				City = "my_fake_city"
			});
			var cnt2 = repo.GetByCity("my_fake_city").Count();

			Assert.AreEqual(1, cnt1);
			Assert.AreEqual(2, cnt2);
		}
	}
}