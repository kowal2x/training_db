namespace Training.Infrastructure
{
	public abstract class QueryBase<TResult>
	{
		public abstract TResult Execute();
	}
}