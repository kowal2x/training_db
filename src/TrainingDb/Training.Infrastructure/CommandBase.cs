namespace Training.Infrastructure
{
	public abstract class CommandBase
	{
		public abstract void Execute();
	}
}