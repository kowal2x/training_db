﻿using System;
using System.IO;
using System.Reflection;
using Telerik.OpenAccess;
using Training.DataAccess.FluentModel;

namespace Training.Infrastructure
{
	public static class ContextFactory
	{
		static ContextFactory()
		{
			var asmPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
			string dbPath = Path.Combine(asmPath, "testdb.db");
			SqliteDbPath = dbPath;
		}

		private static readonly Func<OpenAccessContext> Mssql = () =>
		{
			var backend = new BackendConfiguration()
			{
				Backend = "MsSql",
			};
			return new TrainingContext(
				@"data source=.\SQLEXPRESS;initial catalog=FluentDb;integrated security=True",
				backend,
				new FluentModelMetadataSource()
				);
		};

		private static readonly string SqliteDbPath;
		public static void _removeSqliteDb()
		{
			if (File.Exists(SqliteDbPath))
				File.Delete(SqliteDbPath);
		}

		private static readonly Func<OpenAccessContext> Sqlite = () =>
		{
			var backend = new BackendConfiguration()
			{
				Backend = "sqlite",
			};
			return new TrainingContext(
				string.Format(@"Data Source={0};Version=3;", SqliteDbPath),
				backend,
				new FluentModelMetadataSource()
				);
		};

		private static readonly Func<OpenAccessContext> Default = Sqlite;
		private static Func<OpenAccessContext> _current = Default;
		public static OpenAccessContext Current { get { return _current(); } }

		public static void _replaceLogic(Func<OpenAccessContext> newLogic)
		{
			_current = newLogic;
		}

		public static void _revertToDefault()
		{
			_current = Default;
		}

		public static void _replaceWithSqlite()
		{
			_current = Sqlite;
		}

		public static void _replaceWithMssql()
		{
			_current = Mssql;
		}

		public static OpenAccessContext CreateContext()
		{
			return Current;
		}
	}
}
