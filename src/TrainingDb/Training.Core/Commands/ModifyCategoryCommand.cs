﻿using System.Linq;
using AutoMapper;
using Training.DataAccess.FluentModel;
using Training.Infrastructure;

namespace Training.Core.Commands
{
	public class ModifyCategoryCommand : CommandBase
	{
		private readonly int _categoryId;
		private readonly string _name;
		private readonly string _description;

		public ModifyCategoryCommand(int categoryId, string name, string description)
		{
			_categoryId = categoryId;
			_name = name;
			_description = description;
		}

		public override void Execute()
		{
			using (var ctx = ContextFactory.CreateContext())
			{
				var cat = ctx.GetAll<Category>()
					.Single(e => e.Id == _categoryId);
				cat.Name = _name;
				cat.Description = _description;
				ctx.SaveChanges();
			}
		}
	}
}