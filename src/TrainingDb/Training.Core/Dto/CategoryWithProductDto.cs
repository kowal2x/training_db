﻿using System.Collections.Generic;

namespace Training.Core.Dto
{
	public class CategoryWithProductDto
	{
		public int Id { get; set; }

		public string Name { get; set; }
		public string Description { get; set; }

		public IEnumerable<ProductDto> Products { get; set; }
	}
}