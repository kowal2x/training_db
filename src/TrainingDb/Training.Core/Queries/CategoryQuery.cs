﻿using System.Collections.Generic;
using System.Linq;
using Training.DataAccess.FluentModel;
using Training.Infrastructure;

namespace Training.Core.Queries
{
	public class CategoryQuery : QueryBase<Category>
	{
		private readonly int _categoryId;

		public CategoryQuery(int categoryId)
		{
			_categoryId = categoryId;
		}

		public override Category Execute()
		{
			using (var ctx = ContextFactory.CreateContext())
			{
				return ctx.GetAll<Category>()
					.Single(e => e.Id == _categoryId);
			}
		}
	}
}