﻿using System.Collections.Generic;
using System.Linq;
using Training.DataAccess.FluentModel;
using Training.Infrastructure;

namespace Training.Core.Queries
{
	public class CategoriesQuery : QueryBase<IEnumerable<Category>>
	{
		public override IEnumerable<Category> Execute()
		{
			using (var ctx = ContextFactory.CreateContext())
			{
				return ctx.GetAll<Category>()
					.OrderBy(e => e.Name).ToList();
			}
		}
	}
}