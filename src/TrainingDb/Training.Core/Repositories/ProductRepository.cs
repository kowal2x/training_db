﻿using System.Collections.Generic;
using System.Linq;
using Training.Core.Dto;
using Training.DataAccess.FluentModel;
using Training.Infrastructure;

namespace Training.Core.Repositories
{
	public class ProductRepository : IProductRepository
	{
		public void Add(ProductDto dto)
		{
			using (var ctx = ContextFactory.CreateContext())
			{
				var prod = new Product()
				{
					Name = dto.Name,
					Description = dto.Description,
					Price = dto.Price
				};
				ctx.Add(prod);
				ctx.SaveChanges();
				dto.Id = prod.Id;
			}
		}

		public void Update(ProductDto dto)
		{
			using (var ctx = ContextFactory.CreateContext())
			{
				var prod = ctx.GetAll<Product>()
					.Single(e => e.Id == dto.Id);
				prod.Name = dto.Name;
				prod.Description = dto.Description;
				prod.Price = dto.Price;
				ctx.SaveChanges();
			}
		}

		public void Delete(ProductDto dto)
		{
			using (var ctx = ContextFactory.CreateContext())
			{
				var prod = ctx.GetAll<Product>()
					.Single(e => e.Id == dto.Id);
				ctx.Delete(prod);
				ctx.SaveChanges();
			}
		}

		public IEnumerable<ProductDto> GetProducts()
		{
			using (var ctx = ContextFactory.CreateContext())
			{
				return ctx.GetAll<Product>()
					.Select(e => new ProductDto()
					{
						Id = e.Id,
						Name = e.Name,
						Description = e.Description,
						Price = e.Price
					})
					.ToList();
			}
		}

		public void AttachCategory(ProductDto dto, CategoryDto category)
		{
			using (var ctx = ContextFactory.CreateContext())
			{
				var prod = ctx.GetAll<Product>()
					.Single(e => e.Id == dto.Id);
				prod.Category = ctx.GetAll<Category>().Single(e => e.Id == category.Id);
				ctx.SaveChanges();
			}
		}
	}
}