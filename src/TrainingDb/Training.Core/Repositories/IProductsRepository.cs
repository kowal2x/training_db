﻿using System.Collections;
using System.Collections.Generic;
using Training.Core.Dto;

namespace Training.Core.Repositories
{
	public interface IProductRepository
	{
		void Add(ProductDto dto);
		void Update(ProductDto dto);
		void Delete(ProductDto dto);

		IEnumerable<ProductDto> GetProducts();
		void AttachCategory(ProductDto dto, CategoryDto category);
	}
}