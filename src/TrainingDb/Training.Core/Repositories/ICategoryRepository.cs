﻿using System.Collections.Generic;
using Training.Core.Dto;

namespace Training.Core.Repositories
{
	public interface ICategoryRepository
	{
		void Add(CategoryDto dto);
		void Update(CategoryDto dto);
		void Delete(CategoryDto dto);

		IEnumerable<CategoryDto> GetCategories();
		IEnumerable<CategoryWithProductDto> GetCategoriesWithAvailableProducts();
	}
}