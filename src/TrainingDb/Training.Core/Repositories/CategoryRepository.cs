﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Telerik.OpenAccess;
using Training.Core.Dto;
using Training.DataAccess.FluentModel;
using Training.Infrastructure;

namespace Training.Core.Repositories
{
	public class CategoryRepository : ICategoryRepository
	{
		public void Add(CategoryDto dto)
		{
			using (var ctx = ContextFactory.CreateContext())
			{
				var cat = Mapper.Map<CategoryDto, Category>(dto);
				ctx.Add(cat);
				ctx.SaveChanges();
				dto.Id = cat.Id;
			}
		}

		public void Update(CategoryDto dto)
		{
			using (var ctx = ContextFactory.CreateContext())
			{
				var cat = ctx.GetAll<Category>()
					.Single(e => e.Id == dto.Id);
				Mapper.Map(dto, cat);
				ctx.SaveChanges();
			}
		}

		public void Delete(CategoryDto dto)
		{
			using (var ctx = ContextFactory.CreateContext())
			{
				var cat = ctx.GetAll<Category>()
					.Single(e => e.Id == dto.Id);
				ctx.Delete(cat);
				ctx.SaveChanges();
			}
		}

		public IEnumerable<CategoryDto> GetCategories()
		{
			using (var ctx = ContextFactory.CreateContext())
			{
				return Mapper.Map<IEnumerable<Category>, IEnumerable<CategoryDto>>(
					ctx.GetAll<Category>()).ToList();
			}
		}

		public IEnumerable<CategoryWithProductDto> GetCategoriesWithAvailableProducts()
		{
			using (var ctx = ContextFactory.CreateContext())
			{
				var cats = ctx.GetAll<Category>()
					.Include(c => c.Products)
					.Select(e => new
					{
						Category = e,
						Products = e.Products.Where(f => f.IsAvailable)
					}).ToList();
				return cats.Select(e =>
				{
					var el = Mapper.Map<Category, CategoryWithProductDto>(e.Category);
					el.Products = Mapper.Map<IEnumerable<Product>, IEnumerable<ProductDto>>(e.Products);
					return el;
				}).ToList();
			}
		}
	}
}