﻿using AutoMapper;
using Training.Core.Dto;
using Training.DataAccess.FluentModel;

namespace Training.Core
{
	public class AutoMapperBootstrapper
	{
		public static void Initialize()
		{
			Mapper.CreateMap<Product, ProductDto>()
				//ForMember(e => e.CategoryName, opt => opt.ResolveUsing(f => f.Category.Name + f.Category.Description))
				.ForMember(e => e.CategoryName, opt => opt.MapFrom(f => f.Category.Name))
				.ForMember(e => e.CategoryDescription, opt => opt.MapFrom(f => f.Category.Description));
			Mapper.CreateMap<ProductDto, Product>()
				.ForMember(e => e.Id, opt => opt.Ignore());

			Mapper.CreateMap<Category, CategoryDto>();
			Mapper.CreateMap<CategoryDto, Category>()
				.ForMember(e => e.Id, opt => opt.Ignore());
			Mapper.CreateMap<Category, CategoryWithProductDto>()
				.ForMember(e => e.Products, opt => opt.Ignore());
		}
	}
}