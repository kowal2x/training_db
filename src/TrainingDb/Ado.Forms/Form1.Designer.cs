﻿namespace Ado.Forms
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.dataGridView1 = new System.Windows.Forms.DataGridView();
			this.albumIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.genreIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.artistIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.titleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.priceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.albumArtUrlDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.albumsBindingSource = new System.Windows.Forms.BindingSource(this.components);
			this.mvcMusicStoreDataSet = new Ado.Forms.MvcMusicStoreDataSet();
			this.albumsTableAdapter = new Ado.Forms.MvcMusicStoreDataSetTableAdapters.AlbumsTableAdapter();
			this.button1 = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.albumsBindingSource)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.mvcMusicStoreDataSet)).BeginInit();
			this.SuspendLayout();
			// 
			// dataGridView1
			// 
			this.dataGridView1.AutoGenerateColumns = false;
			this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.albumIdDataGridViewTextBoxColumn,
            this.genreIdDataGridViewTextBoxColumn,
            this.artistIdDataGridViewTextBoxColumn,
            this.titleDataGridViewTextBoxColumn,
            this.priceDataGridViewTextBoxColumn,
            this.albumArtUrlDataGridViewTextBoxColumn});
			this.dataGridView1.DataSource = this.albumsBindingSource;
			this.dataGridView1.Location = new System.Drawing.Point(12, 73);
			this.dataGridView1.Name = "dataGridView1";
			this.dataGridView1.Size = new System.Drawing.Size(982, 459);
			this.dataGridView1.TabIndex = 0;
			// 
			// albumIdDataGridViewTextBoxColumn
			// 
			this.albumIdDataGridViewTextBoxColumn.DataPropertyName = "AlbumId";
			this.albumIdDataGridViewTextBoxColumn.HeaderText = "AlbumId";
			this.albumIdDataGridViewTextBoxColumn.Name = "albumIdDataGridViewTextBoxColumn";
			this.albumIdDataGridViewTextBoxColumn.ReadOnly = true;
			// 
			// genreIdDataGridViewTextBoxColumn
			// 
			this.genreIdDataGridViewTextBoxColumn.DataPropertyName = "GenreId";
			this.genreIdDataGridViewTextBoxColumn.HeaderText = "GenreId";
			this.genreIdDataGridViewTextBoxColumn.Name = "genreIdDataGridViewTextBoxColumn";
			// 
			// artistIdDataGridViewTextBoxColumn
			// 
			this.artistIdDataGridViewTextBoxColumn.DataPropertyName = "ArtistId";
			this.artistIdDataGridViewTextBoxColumn.HeaderText = "ArtistId";
			this.artistIdDataGridViewTextBoxColumn.Name = "artistIdDataGridViewTextBoxColumn";
			// 
			// titleDataGridViewTextBoxColumn
			// 
			this.titleDataGridViewTextBoxColumn.DataPropertyName = "Title";
			this.titleDataGridViewTextBoxColumn.HeaderText = "Title";
			this.titleDataGridViewTextBoxColumn.Name = "titleDataGridViewTextBoxColumn";
			// 
			// priceDataGridViewTextBoxColumn
			// 
			this.priceDataGridViewTextBoxColumn.DataPropertyName = "Price";
			this.priceDataGridViewTextBoxColumn.HeaderText = "Price";
			this.priceDataGridViewTextBoxColumn.Name = "priceDataGridViewTextBoxColumn";
			// 
			// albumArtUrlDataGridViewTextBoxColumn
			// 
			this.albumArtUrlDataGridViewTextBoxColumn.DataPropertyName = "AlbumArtUrl";
			this.albumArtUrlDataGridViewTextBoxColumn.HeaderText = "AlbumArtUrl";
			this.albumArtUrlDataGridViewTextBoxColumn.Name = "albumArtUrlDataGridViewTextBoxColumn";
			// 
			// albumsBindingSource
			// 
			this.albumsBindingSource.DataMember = "Albums";
			this.albumsBindingSource.DataSource = this.mvcMusicStoreDataSet;
			this.albumsBindingSource.CurrentChanged += new System.EventHandler(this.albumsBindingSource_CurrentChanged);
			// 
			// mvcMusicStoreDataSet
			// 
			this.mvcMusicStoreDataSet.DataSetName = "MvcMusicStoreDataSet";
			this.mvcMusicStoreDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
			// 
			// albumsTableAdapter
			// 
			this.albumsTableAdapter.ClearBeforeFill = true;
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(13, 13);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 23);
			this.button1.TabIndex = 1;
			this.button1.Text = "button1";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1006, 544);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.dataGridView1);
			this.Name = "Form1";
			this.Text = "Form1";
			this.Load += new System.EventHandler(this.Form1_Load);
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.albumsBindingSource)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.mvcMusicStoreDataSet)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.DataGridView dataGridView1;
		private MvcMusicStoreDataSet mvcMusicStoreDataSet;
		private System.Windows.Forms.BindingSource albumsBindingSource;
		private MvcMusicStoreDataSetTableAdapters.AlbumsTableAdapter albumsTableAdapter;
		private System.Windows.Forms.DataGridViewTextBoxColumn albumIdDataGridViewTextBoxColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn genreIdDataGridViewTextBoxColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn artistIdDataGridViewTextBoxColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn titleDataGridViewTextBoxColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn priceDataGridViewTextBoxColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn albumArtUrlDataGridViewTextBoxColumn;
		private System.Windows.Forms.Button button1;
	}
}

