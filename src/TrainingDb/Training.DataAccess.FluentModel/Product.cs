﻿using System.Collections;
using Telerik.OpenAccess.Metadata;
using Telerik.OpenAccess.Metadata.Fluent;

namespace Training.DataAccess.FluentModel
{
	public class Product
	{
		public int Id { get; set; }

		public string Name { get; set; }
		public string Description { get; set; }
		public decimal Price { get; set; }
		public bool IsAvailable { get; set; }

		public int CategoryId { get; set; }
		public Category Category { get; set; }

		public static MappingConfiguration GetMapping()
		{
			var productConfiguration = new MappingConfiguration<Product>();
			productConfiguration.MapType(p => new
			{
				p.Id,
				p.Name,
				p.Description,
				p.Price,
				p.CategoryId,
				p.IsAvailable
			}).ToTable("Products");
			productConfiguration
				.HasProperty(p => p.Id)
				.IsIdentity(KeyGenerator.Autoinc);
			productConfiguration
				.HasProperty(p => p.Description)
				.HasLength(8000);

			//productConfiguration
			//	.HasAssociation(c => c.Category)
			//	.WithOpposite(p => p.Products)
			//	.HasConstraint((p,  c) => c.Id == p.CategoryId);

			return productConfiguration;
		}
	}
}