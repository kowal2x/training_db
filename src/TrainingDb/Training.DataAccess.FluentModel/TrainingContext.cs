using Telerik.OpenAccess;
using Telerik.OpenAccess.Metadata;

namespace Training.DataAccess.FluentModel
{
	public class TrainingContext : OpenAccessContext
	{
		public TrainingContext(string connectionString, BackendConfiguration backendConfiguration, MetadataSource metadataSource)
			: base(connectionString, backendConfiguration, metadataSource)
		{
		}
	}
}