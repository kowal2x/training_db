﻿using System.Collections.Generic;
using Telerik.OpenAccess.Metadata;
using Telerik.OpenAccess.Metadata.Fluent;

namespace Training.DataAccess.FluentModel
{
	public class Category
	{
		public Category()
		{
			Products = new List<Product>();
		}

		public int Id { get; set; }

		public string Name { get; set; }
		public string Description { get; set; }

		public IList<Product> Products { get; set; }

		public static MappingConfiguration GetMapping()
		{
			var categoryConfiguration = new MappingConfiguration<Category>();
			categoryConfiguration.MapType(p => new
			{
				p.Id,
				p.Name,
				p.Description,
			}).ToTable("Categories");
			categoryConfiguration
				.HasProperty(p => p.Id)
				.IsIdentity(KeyGenerator.Autoinc);
			categoryConfiguration
				.HasAssociation(c => c.Products)
				.WithOpposite(p => p.Category)
				.HasConstraint((c, p) => c.Id == p.CategoryId);
			return categoryConfiguration;
		}
	}
}