#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by the ClassGenerator.ttinclude code generation file.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
namespace Training.DataAccess.Data.Entities	
{
	public partial class OrderDetail
	{
		private int _orderDetailId;
		public virtual int OrderDetailId
		{
			get
			{
				return this._orderDetailId;
			}
			set
			{
				this._orderDetailId = value;
			}
		}
		
		private int _orderId;
		public virtual int OrderId
		{
			get
			{
				return this._orderId;
			}
			set
			{
				this._orderId = value;
			}
		}
		
		private int _albumId;
		public virtual int AlbumId
		{
			get
			{
				return this._albumId;
			}
			set
			{
				this._albumId = value;
			}
		}
		
		private int _quantity;
		public virtual int Quantity
		{
			get
			{
				return this._quantity;
			}
			set
			{
				this._quantity = value;
			}
		}
		
		private decimal _unitPrice;
		public virtual decimal UnitPrice
		{
			get
			{
				return this._unitPrice;
			}
			set
			{
				this._unitPrice = value;
			}
		}
		
		private Order _order;
		public virtual Order Order
		{
			get
			{
				return this._order;
			}
			set
			{
				this._order = value;
			}
		}
		
		private Album _album;
		public virtual Album Album
		{
			get
			{
				return this._album;
			}
			set
			{
				this._album = value;
			}
		}
		
	}
}
#pragma warning restore 1591
