﻿using AutoMapper;
using Training.DataAccess.FluentModel;
using Training.WpfSample.NotificationObjects;

namespace Training.WpfSample
{
	public class AutoMapperBootstrapper
	{
		public static void Initialize()
		{
			Mapper.CreateMap<Category, CategoryNO>();
			Mapper.CreateMap<CategoryNO, Category>()
				.ForMember(e => e.Id, opt => opt.Ignore());
		}
	}
}