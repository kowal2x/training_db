﻿using GalaSoft.MvvmLight;

namespace Training.WpfSample.NotificationObjects
{
	public class CategoryNO : ObservableObject
	{
		private string _name;
		private string _description;
		public int Id { get; set; }

		public string Name
		{
			get { return _name; }
			set
			{
				_name = value;
				RaisePropertyChanged();
			}
		}

		public string Description
		{
			get { return _description; }
			set
			{
				_description = value;
				RaisePropertyChanged();
			}
		}
	}
}