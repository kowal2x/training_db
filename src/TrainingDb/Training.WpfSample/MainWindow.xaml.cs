﻿using System.Windows;
using Training.WpfSample.ViewModels;

namespace Training.WpfSample
{
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();
			DataContext = new MainViewModel();
		}
	}
}
