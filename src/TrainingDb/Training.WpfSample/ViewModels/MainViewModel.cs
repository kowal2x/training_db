using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Telerik.OpenAccess;
using Training.DataAccess.FluentModel;
using Training.Infrastructure;

namespace Training.WpfSample.ViewModels
{
	public class MainViewModel : ViewModelBase
	{
		private OpenAccessContext ctx;

		public MainViewModel()
		{
			ContextFactory._replaceWithMssql();
			ctx = ContextFactory.CreateContext();
			ctx.Log = new StringWriter();

			CreateCommand = new RelayCommand(CreateProducts);
			LoadCommand = new RelayCommand(LoadData);
			Categories = new ObservableCollection<Category>();
		}

		public IList<Category> Categories { get; private set; }

		public ICommand CreateCommand { get; private set; }
		public ICommand LoadCommand { get; private set; }

		async void LoadData()
		{
			var data = await Task.Run(() => GetCategories());
			foreach (var cat in data)
				Categories.Add(cat);
		}

		async void CreateProducts()
		{
			await Task.Run(() =>
			{
				for (int i = 0; i < 1000; ++i)
				{
					var cat = new Category()
					{
						Name = "cat",
						Description = "wegre",
					};
					var prod = new Product()
					{
						Name = "aaaa",
						Description = "ddddd",
						Price = 2,
						Category = cat
					};
					ctx.Add(prod);
					ctx.SaveChanges();
				}
			});
			MessageBox.Show("done");
		}

		IEnumerable<Category> GetCategories()
		{
			return ctx.GetAll<Category>();
		}
	}
}