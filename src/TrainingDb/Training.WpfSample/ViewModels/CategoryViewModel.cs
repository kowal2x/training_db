﻿using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using AutoMapper;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Training.Core.Commands;
using Training.Core.Queries;
using Training.DataAccess.FluentModel;
using Training.Infrastructure;
using Training.WpfSample.NotificationObjects;

namespace Training.WpfSample.ViewModels
{
	public class CategoryViewModel : ViewModelBase
	{
		private CategoryNO _category;

		public CategoryViewModel(int categoryId)
		{
			OkCommand = new RelayCommand(OkAction);
			Initialize(categoryId);
		}

		private async void Initialize(int categoryId)
		{
			Category = await Task.Run(
				() =>
				{
					var ret = new CategoryQuery(categoryId).Execute();
					return Mapper.Map<
							Category,
							CategoryNO>(ret);
				});
		}

		public CategoryNO Category
		{
			get { return _category; }
			set
			{
				_category = value;
				RaisePropertyChanged();
			}
		}

		public ICommand OkCommand { get; private set; }
		void OkAction()
		{
			new ModifyCategoryCommand(Category.Id,
				Category.Name, Category.Description).Execute();

			Messenger.Default.Send(new CategoryUpdatedMessage(Category));
		}
	}
}