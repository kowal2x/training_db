﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using AutoMapper;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Training.Core.Queries;
using Training.DataAccess.FluentModel;
using Training.WpfSample.NotificationObjects;

namespace Training.WpfSample.ViewModels
{
	public class CategoriesViewModel : ViewModelBase
	{
		public CategoriesViewModel()
		{
			UpdateCommand = new RelayCommand<CategoryNO>(UpdateAction);
			Categories = new ObservableCollection<CategoryNO>();
			Messenger.Default.Register<CategoryUpdatedMessage>(this, UpdateMessageCallback);
			Initialize();
		}

		void UpdateMessageCallback(CategoryUpdatedMessage msg)
		{
			var cat = Categories.Single(e => e.Id == msg.Content.Id);
			cat.Name = msg.Content.Name;
			cat.Description = msg.Content.Description;
		}

		public IList<CategoryNO> Categories { get; private set; }

		private async void Initialize()
		{
			var categories = await Task.Run(
				() =>
				{
					var result = new CategoriesQuery().Execute();
					return Mapper.Map<
							IEnumerable<Category>,
							IEnumerable<CategoryNO>>(result);
				});
			foreach (var cat in categories)
				Categories.Add(cat);
		}

		public ICommand UpdateCommand { get; private set; }
		void UpdateAction(CategoryNO no)
		{
			var cevm = new CategoryViewModel(no.Id);
			var wnd = new CategoryEditWindow()
			{
				DataContext = cevm
			};
			wnd.Show();
		}
	}
}
