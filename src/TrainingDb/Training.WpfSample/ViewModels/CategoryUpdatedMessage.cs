using GalaSoft.MvvmLight.Messaging;
using Training.WpfSample.NotificationObjects;

namespace Training.WpfSample.ViewModels
{
	class CategoryUpdatedMessage : GenericMessage<CategoryNO>
	{
		public CategoryUpdatedMessage(CategoryNO content)
			: base(content)
		{
		}
	}
}