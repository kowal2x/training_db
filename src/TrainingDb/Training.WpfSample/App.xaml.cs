﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Training.Infrastructure;

namespace Training.WpfSample
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
		protected override void OnStartup(StartupEventArgs e)
		{
			base.OnStartup(e);
			AutoMapperBootstrapper.Initialize();
			ContextFactory._replaceWithMssql();
			using (var ctx = ContextFactory.CreateContext())
			{
				string script = null;
				var schemaHandler = ctx.GetSchemaHandler();
				if (schemaHandler.DatabaseExists() == false)
				{
					script = schemaHandler.CreateDDLScript();
					schemaHandler.CreateDatabase();
				}
				else
					script = schemaHandler.CreateUpdateDDLScript(null);

				if (string.IsNullOrWhiteSpace(script) == false)
					schemaHandler.ExecuteDDLScript(script);
			}
		}
	}
}
